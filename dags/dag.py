import datetime as dt
import random

from airflow import DAG
from airflow.utils.trigger_rule import TriggerRule
from airflow.contrib.operators.gcs_to_bq import GoogleCloudStorageToBigQueryOperator
from airflow.contrib.operators.dataproc_operator import (DataprocClusterCreateOperator,
                                                         DataprocClusterDeleteOperator,
                                                         DataProcPySparkOperator)
from airflow.contrib.operators.dataflow_operator import DataFlowPythonOperator
from airflow.operators.python_operator import BranchPythonOperator
from airflow.operators.dummy_operator import DummyOperator
from godatadriven.operators.postgres_to_gcs import PostgresToGoogleCloudStorageOperator

from http_to_gcs_operator import HttpToGcsOperator

dag = DAG(
    dag_id="my_postgres_dag",
    schedule_interval="30 7 * * *",
    default_args={
        "owner": "airflow",
        "start_date": dt.datetime(2018, 10, 1),
        "email_on_failure": True,
        "email": "airflow_errors@myorganisation.com",
    },
)

pgsql_to_gcs = PostgresToGoogleCloudStorageOperator(
    task_id="postgres_to_gcs_task",
    postgres_conn_id="my_postgres_connection",
    sql="SELECT * FROM land_registry_price_paid_uk WHERE transfer_date = '{{ ds }}'",
    bucket="guido-airflow-training",
    filename="postgres_output/{{ ds }}",
    dag=dag
)

dataproc_create_cluster = DataprocClusterCreateOperator(
    task_id="create_cluster_task",
    cluster_name="analyse-pricing-{{ ds }}",
    project_id="airflowbolcom-99f0be9acf92af45",
    num_workers=2,
    zone="europe-west1-c",
    dag=dag
)

compute_aggregates = DataProcPySparkOperator(
    task_id="spark_task",
    main="gs://europe-west1-training-airfl-f901f616-bucket/other/build_statistics_simple.py",
    cluster_name="analyse-pricing-{{ ds }}",
    arguments=["{{ ds }}"],
    dag=dag
)

dataproc_delete_cluster = DataprocClusterDeleteOperator(
    task_id="delete_cluster_task",
    cluster_name="analyse-pricing-{{ ds }}",
    project_id="airflowbolcom-99f0be9acf92af45",
    trigger_rule=TriggerRule.ALL_DONE,
    dag=dag
)

gcs_to_bq = GoogleCloudStorageToBigQueryOperator(
    task_id="gcs_to_bigquery",
    bucket="guido-airflow-training",
    source_objects=["average_prices/transfer_date={{ ds }}/*.parquet"],
    destination_project_dataset_table="airflowbolcom-99f0be9acf92af45:dataset.prices${{ ds_nodash }}",
    source_format="PARQUET",
    write_disposition="WRITE_TRUNCATE",
    provide_context=True,
    dag=dag
)

currency_gbp_usd_to_gcs = HttpToGcsOperator(
    task_id='currency_gbp_usd_to_gcs',
    http_conn_id='my_http_connection',
    endpoint='airflow-training-transform-valutas/',
    endpoint_params={'date': ' {{ ds }} ', 'from': 'GBP', 'to': 'USD'},
    bucket='guido-airflow-training',
    gcs_path='currencies/{{ ds }}/gbp_usd.json',
    dag=dag
)

currency_gbp_eur_to_gcs = HttpToGcsOperator(
    task_id='currency_gbp_eur_to_gcs',
    http_conn_id='my_http_connection',
    endpoint='airflow-training-transform-valutas/',
    endpoint_params={'date': ' {{ ds }} ', 'from': 'GBP', 'to': 'EUR'},
    bucket='guido-airflow-training',
    gcs_path='currencies/{{ ds }}/gbp_eur.json',
    dag=dag
)

dataflow_to_bigquery = DataFlowPythonOperator(
    task_id='dataflow_to_bigquery',
    dataflow_default_options={
        "region": "europe-west1",
        "project": "airflowbolcom-99f0be9acf92af45",
        "input": "gs://guido-airflow-training/postgres_output/*",
        "table": "dataflow_output",
        "dataset": "dataset",
        "bucket": "europe-west1-training-airfl-f901f616-bucket",
        "name": "write-to-bq-{{ ds }}"
    },
    py_file="gs://airflow-training-data/dataflow_job.py",
    dag=dag
)

pgsql_to_gcs >> dataproc_create_cluster >> compute_aggregates >> dataproc_delete_cluster >> gcs_to_bq >> currency_gbp_eur_to_gcs >> currency_gbp_usd_to_gcs >> dataflow_to_bigquery


day_dag = DAG(
    dag_id="my-day_dag",
    schedule_interval="30 7 * * *",
    default_args={
        "owner": "airflow",
        "start_date": dt.datetime(2018, 10, 1),
        "email_on_failure": True,
        "email": "airflow_errors@myorganisation.com",
    },
)

days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']


def get_day_of_week(**kwargs):
    weekday_index = kwargs.get('execution_date').weekday()
    return days[weekday_index]


branching = BranchPythonOperator(
    task_id='branching',
    python_callable=get_day_of_week,
    dag=day_dag,
    provide_context=True
)

join = DummyOperator(
    task_id='join',
    trigger_rule=TriggerRule.ONE_SUCCESS,
    dag=day_dag
)

for day in days:
    branching >> DummyOperator(task_id=day, dag=day_dag) >> join
