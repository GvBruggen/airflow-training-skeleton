from airflow.hooks.http_hook import HttpHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.contrib.hooks.gcs_hook import GoogleCloudStorageHook
import json
import tempfile

class HttpToGcsOperator(BaseOperator):
    """
    Calls an endpoint on an HTTP system to execute an action

    :param http_conn_id: The connection to run the operator against
    :type http_conn_id: string
    :param endpoint: The relative part of the full url. (templated)
    :type endpoint: string
    :param gcs_path: The path of the GCS to store the result
    :type gcs_path: string
    """

    template_fields = ('endpoint_params', 'gcs_path')
    ui_color = '#f4a460'

    @apply_defaults
    def __init__(self,
                 http_conn_id, endpoint, endpoint_params, bucket, gcs_path,
                 *args, **kwargs):
        super(HttpToGcsOperator, self).__init__(*args, **kwargs)
        self.http_conn_id = http_conn_id
        self.endpoint = endpoint
        self.endpoint_params = endpoint_params
        self.bucket = bucket
        self.gcs_path = gcs_path

    def execute(self, context):
        http_hook = HttpHook(method='GET', http_conn_id=self.http_conn_id)
        response = http_hook.run(endpoint=self.endpoint, data=self.endpoint_params)

        with tempfile.NamedTemporaryFile() as tmp_file:
            tmp_file.write(response.content)
            tmp_file.flush()

            gcs_hook = GoogleCloudStorageHook()
            gcs_hook.upload(self.bucket, self.gcs_path, tmp_file.name, mime_type='application/json')
